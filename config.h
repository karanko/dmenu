/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Consolas:style=Regular:size=12",
	"Charcoal:size=12",
	"Terminus:style=Medium:size=12",
	"Droid Sans:size=12",
	"NotoMono Nerd Font Mono:size=12",
	"JetBrains Mono,JetBrains Mono:size=12",
	"Segoe UI:size=12",
	"monospace:size=12"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */

static const char col_gray1[] = "#1e1e1e";
static const char col_gray2[] = "#23edda";
static const char col_gray3[] = "#dedede";
static const char col_gray4[] = "#323232";
static const char col_black[] = "#000000";
static const char col_cyan[] = "#9c9cff";


static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = {col_gray3, col_gray1, col_gray1},
	[SchemeSel] = {col_gray4, col_cyan, col_cyan},
	[SchemeOut] = {col_gray1, col_gray1, col_gray1},
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 10;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* -n option; preselected item starting from 0 */
static unsigned int preselected = 0;
